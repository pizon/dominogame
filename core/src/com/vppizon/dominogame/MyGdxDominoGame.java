package com.vppizon.dominogame;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.Scanner;
public class MyGdxDominoGame extends ApplicationAdapter {
	SpriteBatch batch;
	Domino domino;
	private int dominoNumber1;
	private int dominoNumber2;

	@Override
	public void create () {
		batch = new SpriteBatch();
		domino = new Domino(dominoNumber1, dominoNumber2);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		domino.draw(batch, 50, 130);
		batch.end();
	}

	@Override
	public void dispose () {
		batch.dispose();
		domino.dispose();
	}
}
