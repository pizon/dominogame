package com.vppizon.dominogame;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Basic domino class. Contains both drawing and gaming logic.
 */
public class Domino {

    private int mNumber1;
    private int mNumber2;
    private Texture img1;
    private Texture img2;

    public Domino(int number1, int number2) {
        mNumber1 = number1;
        mNumber2 = number2;
        img1 = getTexture(number1);
        img2 = getTexture(number2);
    }

    /**
     * Draws a domino on canvas.
     * @param batch the {@link SpriteBatch} used to perform canvas draw-s
     * @param x start X- coordinate (low left corner)
     * @param y start Y- coordinate (low left corner)
     */
    public void draw(SpriteBatch batch, float x, float y) {
        batch.draw(img1, x, y);
        batch.draw(img2, x + img1.getWidth() - 2, y);
    }

    /**
     * Dispose of image resources.
     */
    public void dispose() {
        img1.dispose();
        img2.dispose();
    }

    private Texture getTexture(int number) {
        return new Texture("domino-" + number + ".png");
    }


}
